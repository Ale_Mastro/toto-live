import profile from "../assets/images/kemal.jpg";
import logo from "../assets/images/logo.png";
import footballPlayer from "../assets/images/footballPlayer.png";

export default {
  profile,
  logo,
  footballPlayer
};
