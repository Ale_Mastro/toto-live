import { useState } from 'react';
import { SafeAreaView, ScrollView, View, Image, Text, Dimensions } from 'react-native';
import { Stack, useRouter } from 'expo-router';
import Carousel from 'react-native-reanimated-carousel';

import { COLORS, icons, images, SIZES } from '../constants';
import { Nearbyjobs, Popularjobs, ScreenHeaderBtn, Welcome } from '../components';

const Home = () => {
  let width = Dimensions.get('window').width;

  const router = useRouter();
  const [searchTerm, setSearchTerm] = useState('');
  const [pagination, setPagination] = useState(0);
  const pages = [icons.trophy, icons.dice, icons.home, icons.userCircle, icons.cog];
  return (
    <SafeAreaView style={{ flex: 1, background: 'linear-gradient(0deg, #E7E7E7, #E7E7E7), rgba(75, 230, 192, 0.2)', boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)' }}>
      <Stack.Screen
        options={{
          headerStyle: { backgroundColor: COLORS.primary },
          headerShadowVisible: false,
          headerLeft: () => (
            <Image
              source={images.logo}
              resizeMode="cover"
              style={{ margin: 10 }}
              // style={styles.btnImg(dimension)}
            />
            // <ScreenHeaderBtn iconUrl={} dimension='100%' />
          ),
          headerTitle: '',
        }}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            flex: 1,
            paddingVertical: SIZES.medium,
          }}>
          <div
            style={{
              position: 'relative',
              display: 'flex',
              width: '90%',
              height: 153,
              background: 'linear-gradient(269.97deg, #187E7F 0.02%, #9DBAB3 99.97%, #393262 99.98%)',
              // border: "1px solid rgba(255, 255, 255, 0.14)",
              borderRadius: 24,
              margin: 'auto',
            }}>
            <View style={{ width: '100%', height: '100%', position: 'relative', overflowY: 'visible', overflowX: 'clip' }}>
              <Carousel
                loop
                width={width}
                // height={width / 2}
                style={{ border: 'none', width: '100%', height: '100%', position: 'relative', overflow: 'visible' }}
                autoPlay={true}
                data={[...new Array(3).keys()]}
                scrollAnimationDuration={1000}
                onSnapToItem={(index) => setPagination(index)}
                onScrollStart={(index) => setPagination(index)}
                renderItem={({ index }) => (
                  <View
                    style={{
                      display: 'flex',
                      height: '100%',
                      // width:"390",
                      justifyContent: 'center',
                      overflow: 'visible',
                    }}>
                    <div style={{ position: 'relative', width: '50%' }}></div>
                    <div style={{ alignItems: 'center', display: 'flex', width: '50%', justifyContent: 'center' }}>
                      <div style={{ textAlign: 'center', color: 'white' }}>
                        <Text style={{ fontWeight: 'bold', color: 'white' }}>Giornata 8 di 38</Text>
                        <br />
                        <Text style={{ color: 'white' }}>Mario è primo nel torneo BestClub</Text>
                        <Image source={images.footballPlayer} style={{ top: '-15px', right: 40, position: 'absolute' }} />
                      </div>
                    </div>
                  </View>
                )}
              />
            </View>
            <div
              style={{
                position: 'absolute',
                bottom: 5,
                display: 'flex',
                justifyContent: 'center',
                gap: '5px',
                left: 0,
                right: 0,
              }}>
              {[0, 1, 2].map((index) => (
                <div style={{ border: index === pagination ? '1px solid #187E7F' : '1px solid #FFFFFF', borderRadius: 100, height: 5, width: 5, background: index === pagination ? '#187E7F' : '#FFFFFF' }}></div>
              ))}
            </div>
          </div>
          {/* <Welcome
            searchTerm={searchTerm}
            setSearchTerm={setSearchTerm}
            handleClick={() => {
              if (searchTerm) {
                router.push(`/search/${searchTerm}`)
              }
            }}
          /> */}

          {/* <Popularjobs />
          <Nearbyjobs /> */}
        </View>
        <div style={{
          display:"flex",
          justifyContent:"space-between",
          margin: "20px auto",
          width: '90%',

        }}>
        <Text style={{
          fontFamily: 'Open Sans',
          fontStyle: "normal",
          fontWeight: 749,
          fontSize: 21,
          lineHeight: "105%",
          /* or  */
          
          textAlign: "center",
          letterSpacing: "-0.408px",
          textTransform: "capitalize",
          
          color:"#423232",
          fontStretch: 100,
        }}>Campionati in Corso</Text>
        <Text style={{
          fontFamily: 'Open Sans',
          fontStyle: "normal",
          fontWeight: 800,
          fontSize: 13,
          lineHeight: "169%",
          /* or  */
          
          textAlign: "center",
          letterSpacing: "-0.408px",
          textTransform: "capitalize",
          
          color:"#888888",
          fontStretch: 100,
        }}>VIEW ALL</Text>

        </div>
      </ScrollView>
      <div style={{ display: 'flex', justifyContent: 'space-around', alignItems: 'center', height: 50, width: '100%', background: '#187E7F' }}>
        {pages.map((title) => (
          <Image key={title} source={title} />
        ))}
      </div>
    </SafeAreaView>
  );
};

export default Home;
